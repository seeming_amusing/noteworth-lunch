# Noteworth Lunch

As busy developers, it is important to be able to quickly find a place to eat when starving. Often times, the
search takes too long due to the overwhelming possible selections available. With this app, we aim to resolve
your hunger by showing you only the restaurants you want.

## Architecture and Design

This project is designed for simplicity of components and scalability in testing. It borrows many of the
concepts for MVI (as per [this blog series](http://hannesdorfmann.com/android/mosby3-mvi-1)) and Clean Code
(as per [this blog post](https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html)).

The following packages play a unique role in the structure of the code:

* `api`: Represents the network / storage layer. Any data that would need to be fetched and/or stored should be placed here.
* `data`: Represents the common business logic behind the data. This should contain components responsible for converting raw API data into the appropriate object for handling.
* `presentation`: Represents the view layer. This layer is also broken down further by screens, to allow better modularization of each screen.

The presentation layer utilizes a `MVP` structure, while borrowing concepts for `MVI`. This results in a
`MVP+I` implementation that allows for an easier transition to true MVI, when necessary. Until such a
transition, it should still be possible to keep components small and simple, while providing some of the
ease of debugging.

## Planned Features and Functionality

The following are some items planned for addition, once time is found:

* Dagger 2 integration: Currently a faux DI system is being used to integrate everything. A proper DI system would be first priority, as it removes the need to manually code up the wiring.
* Location screen: Currently, this is hard-coded to a single location (Hoboken, NJ). Once implemented, this would also serve as the onboarding screen
* Restaurant Details screen improvements: The following are some adjustments to help users decide:
    * Restaurant photo gallery (via `ViewPager`)
    * Reviews section
    * Non-lorem ipsum restaurant descriptions (whenever Google Places API supports this)
* Restaurant Listing screen improvements: The following are some adjustments to help users choose the right restaurant:
    * Keyword searches
    * Distance ranges (via `SeekBar`)
    * Improve Sort by UI
    * Restaurant category filters (whenever Google Places API supports this)
  
## Random Links

* LinkedIn ([url](https://www.linkedin.com/in/ericmylee/))
* Sample Braintree SDK integration ([url](https://github.com/seeming-amusing/braintree-sample))
* Sample Adyen SDK integration ([url](https://github.com/seeming-amusing/adyen-integration))

## License
[Apache 2.0](LICENSE)