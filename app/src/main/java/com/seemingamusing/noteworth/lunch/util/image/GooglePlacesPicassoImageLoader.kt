package com.seemingamusing.noteworth.lunch.util.image

import android.content.Context
import android.widget.ImageView
import com.seemingamusing.noteworth.lunch.BuildConfig
import com.seemingamusing.noteworth.lunch.model.Size
import com.squareup.picasso.Picasso
import javax.inject.Inject
import kotlin.math.min

class GooglePlacesPicassoImageLoader
@Inject constructor() : ImageLoader {

  companion object {
    private const val MAX_SIZE = 1_600
  }

  override fun loadImage(into: ImageView, imageId: String, withContext: Context, maxSize: Size) {
    Picasso.with(withContext)
        .load(imageId.toPhotoUrl(withSize = maxSize))
        .into(into)
  }

  private fun String.toPhotoUrl(withSize: Size): String {
    val baseUrl =
        "https://maps.googleapis.com/maps/api/place/photo?key=${BuildConfig.PLACES_API_KEY}&photoreference=$this"
    val sizeParam = when {
      withSize.width > 0 -> "&maxwidth=${min(withSize.width, MAX_SIZE)}"
      withSize.height > 0 -> "&maxheight=${min(withSize.height, MAX_SIZE)}"
      else -> throw IllegalStateException("Must provide either width or height")
    }
    return baseUrl + sizeParam
  }

  override fun loadImage(into: ImageView,
                         imageId: String,
                         withContext: Context,
                         withPlaceholder: Int,
                         maxSize: Size) {
    Picasso.with(withContext)
        .load(imageId.toPhotoUrl(withSize = maxSize))
        .placeholder(withPlaceholder)
        .into(into)
  }
}