package com.seemingamusing.noteworth.lunch.presentation.details

import com.seemingamusing.noteworth.lunch.data.RestaurantDataSource
import com.seemingamusing.noteworth.lunch.util.rxjava.Disposables
import com.seemingamusing.noteworth.lunch.util.rxjava.Transformers
import javax.inject.Inject

class RestaurantDetailsPresenter
@Inject constructor(private val view: RestaurantDetails.View,
                    private val restaurantDataSource: RestaurantDataSource,
                    private val disposables: Disposables,
                    private val transformers: Transformers) : RestaurantDetails.Presenter {

  override fun bind() {
    disposables += view.initialScreenIntent()
        .flatMap { restaurantDataSource[it.placeId] }
        .compose(transformers.forSingle())
        .doOnSubscribe { view.render(state = RestaurantDetails.State(isLoading = true)) }
        .subscribe({ view.render(state = RestaurantDetails.State(content = it)) },
                   { view.render(state = RestaurantDetails.State(error = it)) })
  }

  override fun unbind() {
    disposables.clear()
  }
}