package com.seemingamusing.noteworth.lunch.presentation.listing

import com.seemingamusing.noteworth.lunch.data.Sorting
import com.seemingamusing.noteworth.lunch.model.Restaurant
import io.reactivex.Observable

interface RestaurantListing {

  interface View {
    fun initialScreenIntent(): Observable<Unit>
    fun toggleSortingIntent(): Observable<Sorting>
    fun render(state: State)
  }

  interface Presenter {
    fun bind()
    fun unbind()
  }

  data class State(
      val isLoading: Boolean = false,
      val sorting: Sorting = Sorting.PROMINENCE,
      val content: List<Restaurant> = emptyList(),
      val error: Throwable? = null)
}