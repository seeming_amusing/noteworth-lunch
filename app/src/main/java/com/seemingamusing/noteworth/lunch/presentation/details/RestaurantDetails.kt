package com.seemingamusing.noteworth.lunch.presentation.details

import com.seemingamusing.noteworth.lunch.model.Restaurant
import io.reactivex.Single

interface RestaurantDetails {

  interface View {
    fun initialScreenIntent(): Single<Restaurant>
    fun render(state: State)
  }

  interface Presenter {
    fun bind()
    fun unbind()
  }

  data class State(
      val isLoading: Boolean = false,
      val content: Restaurant? = null,
      val error: Throwable? = null)
}