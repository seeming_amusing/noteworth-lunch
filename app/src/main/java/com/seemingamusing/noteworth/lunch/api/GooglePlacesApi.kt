package com.seemingamusing.noteworth.lunch.api

import com.seemingamusing.noteworth.lunch.BuildConfig
import com.seemingamusing.noteworth.lunch.api.model.BrowseResponse
import com.seemingamusing.noteworth.lunch.api.model.DetailsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface PlacesApi {

  @GET(BASE_URL + "nearbysearch" + BASE_QUERY + "&type=restaurant")
  fun browse(@Query("location", encoded = true) latLong: String,
             @Query("rankby") sorting: String,
             @Query("radius") radius: Int? = null,
             @Query("keyword") keyword: String? = null): Single<BrowseResponse>

  @GET(BASE_URL + "details" + BASE_QUERY)
  operator fun get(@Query("placeid") id: String): Single<DetailsResponse>
}

private const val BASE_URL = "/maps/api/place/"
private const val BASE_QUERY = "/json?key=${BuildConfig.PLACES_API_KEY}"
