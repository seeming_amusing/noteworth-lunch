package com.seemingamusing.noteworth.lunch.util.rxjava

class ResponseErrorException(message: String)
  : RuntimeException("Response error: $message") {

  // Prevent expensive (and unnecessary) stack building
  override fun fillInStackTrace() = this
}