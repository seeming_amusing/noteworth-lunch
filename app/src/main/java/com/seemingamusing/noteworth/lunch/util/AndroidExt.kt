package com.seemingamusing.noteworth.lunch.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.support.v4.app.ActivityOptionsCompat
import android.view.View
import java.io.Serializable
import kotlin.reflect.KClass

fun KClass<out Activity>.start(withContext: Context,
                               options: ActivityOptionsCompat? = null,
                               vararg data: Pair<String, Any>) =
    Intent(withContext, this.java)
        .apply { data.forEach { (key, value) -> setExtra(key, value) } }
        .let { withContext.startActivity(it, options?.toBundle()) }

fun Intent.setExtra(key: String, value: Any) {
  when (value) {
    is Int -> putExtra(key, value)
    is Long -> putExtra(key, value)
    is Float -> putExtra(key, value)
    is Double -> putExtra(key, value)
    is String -> putExtra(key, value)
    is Parcelable -> putExtra(key, value)
    is Serializable -> putExtra(key, value)
    else -> IllegalArgumentException("Cannot handle type ${value.javaClass.simpleName}")
  }
}

fun show(vararg view: View) {
  view.forEach { it.visibility = View.VISIBLE }
}

fun hide(vararg view: View) {
  view.forEach { it.visibility = View.GONE }
}
