package com.seemingamusing.noteworth.lunch

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.seemingamusing.noteworth.lunch.api.AddressDeserializer
import com.seemingamusing.noteworth.lunch.api.PlacesApi
import com.seemingamusing.noteworth.lunch.api.RestaurantDeserializer
import com.seemingamusing.noteworth.lunch.data.GooglePlacesDataSource
import com.seemingamusing.noteworth.lunch.data.RestaurantDataSource
import com.seemingamusing.noteworth.lunch.model.Address
import com.seemingamusing.noteworth.lunch.model.Restaurant
import com.seemingamusing.noteworth.lunch.util.image.GooglePlacesPicassoImageLoader
import com.seemingamusing.noteworth.lunch.util.image.ImageLoader
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

////////////////////////////////////////////////
// Temporary DI system
//
// TODO: Convert this to utlize Dagger instead
////////////////////////////////////////////////

val imageLoader: ImageLoader by lazy { GooglePlacesPicassoImageLoader() }

val restaurantDataSource: RestaurantDataSource by lazy { GooglePlacesDataSource(placesApi) }

val placesApi: PlacesApi by lazy { retrofit.create(PlacesApi::class.java) }

val retrofit: Retrofit by lazy {
  Retrofit.Builder()
      .baseUrl("https://maps.googleapis.com")
      .client(okHttpClient)
      .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
      .addConverterFactory(GsonConverterFactory.create(customConverter))
      .build()
}

private const val defaultTimeout = 10L // in seconds
val okHttpClient: OkHttpClient by lazy {
  val builder = OkHttpClient.Builder()
      .connectTimeout(defaultTimeout, TimeUnit.SECONDS)
      .readTimeout(defaultTimeout, TimeUnit.SECONDS)
      .writeTimeout(defaultTimeout, TimeUnit.SECONDS)
  if (BuildConfig.DEBUG) {
    builder.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
  }
  return@lazy builder.build()
}

val customConverter: Gson by lazy {
  val gson = Gson()
  return@lazy GsonBuilder()
      .registerTypeAdapter(Restaurant::class.java, RestaurantDeserializer(gson))
      .registerTypeAdapter(Address::class.java, AddressDeserializer(gson))
      .create()
}
