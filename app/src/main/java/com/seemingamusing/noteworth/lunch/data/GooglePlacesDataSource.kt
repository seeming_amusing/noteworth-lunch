package com.seemingamusing.noteworth.lunch.data

import com.seemingamusing.noteworth.lunch.api.PlacesApi
import com.seemingamusing.noteworth.lunch.api.model.BrowseResponse
import com.seemingamusing.noteworth.lunch.api.model.DetailsResponse
import com.seemingamusing.noteworth.lunch.api.model.Response
import com.seemingamusing.noteworth.lunch.model.Restaurant
import com.seemingamusing.noteworth.lunch.util.rxjava.ResponseErrorException
import io.reactivex.Single
import javax.inject.Inject

class GooglePlacesDataSource
@Inject constructor(private val placesApi: PlacesApi) : RestaurantDataSource {

  companion object {
    private const val LAT_LONG = "40.7362028,-74.0327031"
  }

  override fun browse(sorting: Sorting, keyword: String?): Single<List<Restaurant>> =
      (when (sorting) { // TODO: Make lat-long variable
        Sorting.DISTANCE -> placesApi.browse(LAT_LONG, sorting.value, keyword = keyword)
        else -> placesApi.browse(LAT_LONG, sorting.value, radius = 5000, keyword = keyword)
      })
          .verifyStatus({ (it as BrowseResponse).results }, { emptyList() })

  private fun <T> Single<out Response>.verifyStatus(mapper: (Response) -> T,
                                                    noResults: () -> T) =
      map { response ->
        return@map when (response.status) {
          "OK" -> mapper(response)
          "ZERO_RESULTS", "NOT_FOUND" -> noResults()
          else -> throw ResponseErrorException(response.errorMessage)
        }
      }

  override fun get(id: String): Single<Restaurant> =
      placesApi[id].verifyStatus({ (it as DetailsResponse).result },
                                 { throw ResponseErrorException("No restaurant found") })
}