package com.seemingamusing.noteworth.lunch.model

data class Restaurant(val id: String = "",
                      val placeId: String = "",
                      val name: String = "",
                      val phoneNumber: String = "",
                      val address: Address = Address(),
                      val vicinity: String = "",
                      val photos: List<Photo> = emptyList(),
                      val isOpen: Boolean = false,
                      val openingHours: List<String> = emptyList(),
                      val rating: Float = 0.0f,
                      val priceLevel: Int? = null,
                      val reviewCount: Int = 0) {

  val imageId = photos.firstOrNull()?.id
}

data class Address(val streetNumber: String = "",
                   val streetName: String = "",
                   val city: String = "",
                   val state: String = "",
                   val country: String = "",
                   val zipCode: String = "")

data class Photo(val id: String = "",
                 val size: Size = Size())

data class Size(val width: Int = 0,
                val height: Int = 0)
