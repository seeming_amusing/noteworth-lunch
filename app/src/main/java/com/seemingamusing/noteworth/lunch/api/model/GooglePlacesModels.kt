package com.seemingamusing.noteworth.lunch.api.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.seemingamusing.noteworth.lunch.model.Restaurant

sealed class Response {
  abstract val status: String
  abstract val errorMessage: String
}

data class BrowseResponse(
  @Expose @SerializedName("results") val results: List<Restaurant> = emptyList(),
  @Expose @SerializedName("status") override val status: String = "",
  @Expose @SerializedName("error_message") override val errorMessage: String = "") : Response()

data class DetailsResponse(
  @Expose @SerializedName("result") val result: Restaurant = Restaurant(),
  @Expose @SerializedName("status") override val status: String = "",
  @Expose @SerializedName("error_message") override val errorMessage: String = "") : Response()

data class AddressComponent(
  @Expose @SerializedName("short_name") val shortName: String = "",
  @Expose @SerializedName("long_name") val value: String = "",
  @Expose @SerializedName("types") private val types: List<String> = emptyList()) {

  infix fun isType(key: String) = key in types
}

data class OpeningHours(
  @Expose @SerializedName("open_now") val openNow: Boolean = false,
  @Expose @SerializedName("weekday_text") val hours: List<String> = emptyList())

data class PlacesPhoto(
  @Expose @SerializedName("photo_reference") val reference: String = "",
  @Expose @SerializedName("width") val width: Int = 0,
  @Expose @SerializedName("height") val height: Int = 0)
