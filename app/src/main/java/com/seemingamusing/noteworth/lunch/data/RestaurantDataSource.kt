package com.seemingamusing.noteworth.lunch.data

import com.seemingamusing.noteworth.lunch.model.Restaurant
import io.reactivex.Single

interface RestaurantDataSource {

  fun browse(sorting: Sorting = Sorting.PROMINENCE, keyword: String? = null): Single<List<Restaurant>>

  operator fun get(id: String): Single<Restaurant>
}

enum class Sorting(val value: String) {
  PROMINENCE("prominence"), DISTANCE("distance")
}