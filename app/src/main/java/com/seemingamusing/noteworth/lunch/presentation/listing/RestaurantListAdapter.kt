package com.seemingamusing.noteworth.lunch.presentation.listing

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import com.seemingamusing.noteworth.lunch.R
import com.seemingamusing.noteworth.lunch.model.Restaurant
import com.seemingamusing.noteworth.lunch.model.Size
import com.seemingamusing.noteworth.lunch.util.image.ImageLoader
import kotlinx.android.synthetic.main.card_restaurant.view.*
import javax.inject.Inject

class RestaurantListAdapter
@Inject constructor(private val context: Context,
                    private val imageLoader: ImageLoader)
  : RecyclerView.Adapter<RestaurantListAdapter.RestaurantCard>() {

  private val inflater = LayoutInflater.from(context)

  lateinit var onRestaurantClick: (ImageView, Restaurant) -> Unit

  var restaurants: List<Restaurant> = emptyList()
    set(value) {
      field = value
      notifyDataSetChanged()
    }

  override fun getItemCount(): Int = restaurants.size

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantCard =
      (inflater.inflate(R.layout.card_restaurant, parent, false) as CardView)
          .let { RestaurantCard(it) }

  override fun onBindViewHolder(holder: RestaurantCard, position: Int) {
    val restaurant = restaurants[position]
    holder.itemView.apply {
      setOnClickListener { onRestaurantClick(restaurant_image, restaurant) }
      restaurant.loadImage(into = restaurant_image)
      restaurant_name.text = restaurant.name
      restaurant_rating.rating = restaurant.rating
      restaurant_reviews_count.text = restaurant.reviewsCountString()
      restaurant_vicinity.text = restaurant.vicinity
    }
  }

  private fun Restaurant.loadImage(into: ImageView) {
    imageId?.let { photoId ->
      imageLoader.loadImage(into, imageId = photoId, withContext = context, maxSize = imageSize(),
                            withPlaceholder = R.drawable.placeholder_restaurant)
    }
  }

  private fun imageSize() = context.resources.getDimensionPixelSize(R.dimen.restaurant_listing_image_size)
      .let { size ->
        return@let when (size) {
          !in 1..1600 -> Size(1600, 1600)
          else -> Size(size, size)
        }
      }

  private fun Restaurant.reviewsCountString() =
      context.resources.getQuantityString(R.plurals.reviews_count, reviewCount, reviewCount)

  class RestaurantCard(parent: CardView) : RecyclerView.ViewHolder(parent)
}