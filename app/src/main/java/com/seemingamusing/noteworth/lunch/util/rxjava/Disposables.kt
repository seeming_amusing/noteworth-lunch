package com.seemingamusing.noteworth.lunch.util.rxjava

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

interface Disposables {
  operator fun plusAssign(disposable: Disposable)
  fun clear()
}

class CompositeDisposables : Disposables {

  private val backingDisposable = CompositeDisposable()

  override fun plusAssign(disposable: Disposable) {
    backingDisposable.add(disposable)
  }

  override fun clear() {
    backingDisposable.clear()
  }
}