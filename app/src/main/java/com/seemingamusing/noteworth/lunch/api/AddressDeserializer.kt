package com.seemingamusing.noteworth.lunch.api

import com.google.gson.*
import com.seemingamusing.noteworth.lunch.api.model.AddressComponent
import com.seemingamusing.noteworth.lunch.model.Address
import java.lang.reflect.Type

class AddressDeserializer(private val gson: Gson) : JsonDeserializer<Address> {

  override fun deserialize(json: JsonElement,
                           typeOfT: Type,
                           context: JsonDeserializationContext): Address =
      when (json) {
        !is JsonArray -> Address()
        else -> json.map { gson.fromJson(it, AddressComponent::class.java) }.transform()
      }

  private fun List<AddressComponent>.transform() = fold(Address()) { address, component ->
    return@fold when {
      component isType "street_number" -> address.copy(streetNumber = component.value)
      component isType "route" -> address.copy(streetName = component.value)
      component isType "locality" -> address.copy(city = component.value)
      component isType "administrative_area_level_1" -> address.copy(state = component.value)
      component isType "country" -> address.copy(country = component.shortName)
      component isType "postal_code" -> address.copy(zipCode = component.value)
      else -> address
    }
  }
}