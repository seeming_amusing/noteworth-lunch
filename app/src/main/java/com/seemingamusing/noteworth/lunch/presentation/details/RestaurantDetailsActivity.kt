package com.seemingamusing.noteworth.lunch.presentation.details

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.view.ViewTreeObserver
import android.widget.ImageView
import com.seemingamusing.noteworth.lunch.R
import com.seemingamusing.noteworth.lunch.imageLoader
import com.seemingamusing.noteworth.lunch.model.Photo
import com.seemingamusing.noteworth.lunch.model.Restaurant
import com.seemingamusing.noteworth.lunch.model.Size
import com.seemingamusing.noteworth.lunch.restaurantDataSource
import com.seemingamusing.noteworth.lunch.util.rxjava.CompositeDisposables
import com.seemingamusing.noteworth.lunch.util.rxjava.DefaultTransformers
import com.seemingamusing.noteworth.lunch.util.start
import io.reactivex.Single
import kotlinx.android.synthetic.main.activity_restaurant_details.*
import kotlinx.android.synthetic.main.content_restaurant_details.*
import timber.log.Timber
import javax.inject.Inject

class RestaurantDetailsActivity : AppCompatActivity(), RestaurantDetails.View {

  companion object {
    private const val EXTRA_RESTAURANT_ID = "restaurant-details.id"
    private const val EXTRA_RESTAURANT_NAME = "restaurant-details.name"
    private const val EXTRA_RESTAURANT_IMAGE_ID = "restaurant-details.imageId"

    fun transition(fromView: View,
                   inActivity: Activity,
                   withName: String,
                   restaurantId: String,
                   restaurantName: String,
                   restaurantImageId: String) {
      val options = ActivityOptionsCompat.makeSceneTransitionAnimation(inActivity, fromView, withName)
      RestaurantDetailsActivity::class.start(inActivity, options,
                                             EXTRA_RESTAURANT_ID to restaurantId,
                                             EXTRA_RESTAURANT_NAME to restaurantName,
                                             EXTRA_RESTAURANT_IMAGE_ID to restaurantImageId)
    }
  }

  @Inject lateinit var presenter: RestaurantDetails.Presenter

  private lateinit var initialRestaurantValues: Restaurant

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_restaurant_details)
    extractInitialState()

    // TODO: Inject the following items
    presenter = RestaurantDetailsPresenter(this, restaurantDataSource, CompositeDisposables(),
                                           DefaultTransformers)

    initializeViews()
    presenter.bind()
  }

  private fun extractInitialState() {
    intent.getStringExtra(EXTRA_RESTAURANT_ID)?.let { id ->
      val name = intent.getStringExtra(EXTRA_RESTAURANT_NAME) ?: ""
      val imageId = intent.getStringExtra(EXTRA_RESTAURANT_IMAGE_ID) ?: ""
      initialRestaurantValues = Restaurant(placeId = id, name = name, photos = listOf(Photo(id = imageId)))
    } ?: throw IllegalStateException("Expected a restaurant ID; none was found")
  }

  private fun initializeViews() {
    displayRestaurantName()
    loadImageDelayed()
  }

  private fun displayRestaurantName() {
    setSupportActionBar(toolbar)
    supportActionBar?.let {
      it.setDisplayHomeAsUpEnabled(true)
      it.setDisplayShowTitleEnabled(true)
      it.title = initialRestaurantValues.name
    }
  }

  private fun loadImageDelayed() {
    initialRestaurantValues.loadImage(into = restaurant_image)
    supportPostponeEnterTransition()
    restaurant_image.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
      override fun onPreDraw(): Boolean {
        restaurant_image.viewTreeObserver.removeOnPreDrawListener(this)
        supportStartPostponedEnterTransition()
        return true
      }
    })
  }

  private fun Restaurant.loadImage(into: ImageView) {
    imageId?.let { photoId ->
      imageLoader.loadImage(into, imageId = photoId, withContext = this@RestaurantDetailsActivity,
                            maxSize = Size(width = 1600))
    }
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    return when (item.itemId) {
      android.R.id.home -> true.also { supportFinishAfterTransition() }
      else -> super.onOptionsItemSelected(item)
    }
  }

  override fun onDestroy() = super.onDestroy().also {
    presenter.unbind()
  }

  override fun initialScreenIntent(): Single<Restaurant> = Single.just(initialRestaurantValues)

  override fun render(state: RestaurantDetails.State) {
    Timber.d("State: $state")
    when {
      state.isLoading -> Timber.i("Show loading") // TODO: Show loading state
      state.error != null -> Timber.e(state.error, "Network error") // TODO: Show error state
      state.content != null -> displayContent(state.content)
      else -> Timber.w("Unexpected state") // TODO: Handle no content state
    }
  }

  private fun displayContent(content: Restaurant) {
    restaurant_rating.rating = content.rating
    restaurant_reviews_count.text = content.reviewsCountString()
    restaurant_vicinity.text = content.vicinity
    restaurant_opening_hours.text = content.openingHours.joinToString("\n")
  }

  private fun Restaurant.reviewsCountString() =
      resources.getQuantityString(R.plurals.reviews_count, reviewCount, reviewCount)
}