package com.seemingamusing.noteworth.lunch

import android.app.Application
import timber.log.Timber

class LunchApplication : Application() {

  override fun onCreate() = super.onCreate().also {
    Timber.plant(Timber.DebugTree())
  }
}