package com.seemingamusing.noteworth.lunch.presentation.listing

import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.ImageView
import com.jakewharton.rxbinding2.widget.checkedChanges
import com.seemingamusing.noteworth.lunch.R
import com.seemingamusing.noteworth.lunch.data.Sorting
import com.seemingamusing.noteworth.lunch.imageLoader
import com.seemingamusing.noteworth.lunch.model.Restaurant
import com.seemingamusing.noteworth.lunch.presentation.details.RestaurantDetailsActivity
import com.seemingamusing.noteworth.lunch.restaurantDataSource
import com.seemingamusing.noteworth.lunch.util.hide
import com.seemingamusing.noteworth.lunch.util.rxjava.CompositeDisposables
import com.seemingamusing.noteworth.lunch.util.rxjava.DefaultTransformers
import com.seemingamusing.noteworth.lunch.util.show
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_restaurant_listing.*
import kotlinx.android.synthetic.main.content_error.*
import kotlinx.android.synthetic.main.content_filter.*
import timber.log.Timber
import javax.inject.Inject

class RestaurantListingActivity : AppCompatActivity(), RestaurantListing.View {

  @Inject lateinit var presenter: RestaurantListing.Presenter
  @Inject lateinit var adapter: RestaurantListAdapter

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_restaurant_listing)

    // TODO: Inject the following items
    presenter = RestaurantListingPresenter(this, restaurantDataSource, CompositeDisposables(),
                                           DefaultTransformers)
    adapter = RestaurantListAdapter(this@RestaurantListingActivity, imageLoader)
        .also { it.onRestaurantClick = { view, restaurant -> goTo(restaurant, view) } }

    initializeViews()
    presenter.bind()
  }

  private fun goTo(restaurant: Restaurant, view: ImageView) {
    RestaurantDetailsActivity.transition(fromView = view,
                                         inActivity = this,
                                         withName = getString(R.string.cd_restaurant_image),
                                         restaurantId = restaurant.placeId,
                                         restaurantName = restaurant.name,
                                         restaurantImageId = restaurant.imageId ?: "")
  }

  private fun initializeViews() {
    restaurant_list.layoutManager = LinearLayoutManager(this)
    restaurant_list.adapter = adapter
    initializeBottomSheet()
  }

  private fun initializeBottomSheet() {
    val behavior = initializeBottomSheetBehavior()
    filter_title.setOnClickListener {
      when (behavior.state) {
        BottomSheetBehavior.STATE_COLLAPSED -> behavior.state = BottomSheetBehavior.STATE_EXPANDED
        BottomSheetBehavior.STATE_EXPANDED -> behavior.state = BottomSheetBehavior.STATE_COLLAPSED
      }
    }
  }

  private fun initializeBottomSheetBehavior() = BottomSheetBehavior.from(filter_bottom_sheet).also {
    it.peekHeight = resources.getDimensionPixelSize(R.dimen.filter_peek_height)
    it.state = BottomSheetBehavior.STATE_COLLAPSED
  }

  override fun onDestroy() = super.onDestroy().also {
    presenter.unbind()
  }

  override fun initialScreenIntent(): Observable<Unit> = Observable.just(Unit)

  override fun toggleSortingIntent(): Observable<Sorting> = option_sorting.checkedChanges().map { isChecked ->
    return@map when {
      isChecked -> Sorting.DISTANCE
      else -> Sorting.PROMINENCE
    }
  }

  override fun render(state: RestaurantListing.State) {
    Timber.d("State: $state")
    when {
      state.isLoading -> {
        show(loading_indicator)
        hide(restaurant_list, error_message)
      }
      state.error != null -> {
        show(error_message)
        hide(restaurant_list, loading_indicator)
        displayError(state.error)
      }
      state.content.isNotEmpty() -> {
        show(restaurant_list)
        hide(loading_indicator, error_message)
        resetList(content = state.content)
      }
      else -> {
        show(error_message)
        hide(restaurant_list, loading_indicator)
        displayEmpty()
      }
    }
  }

  private fun resetList(content: List<Restaurant>) {
    adapter.restaurants = content
    restaurant_list.scrollToPosition(0)
  }

  private fun displayError(error: Throwable) {
    Timber.e(error, "Network error")
    error_icon.setImageResource(R.drawable.ic_error_indicator)
    error_text.setText(R.string.error_network)
  }

  private fun displayEmpty() {
    Timber.w("No content available")
    error_icon.setImageResource(R.drawable.ic_no_restaurants)
    error_text.setText(R.string.error_no_restaurants)
  }
}