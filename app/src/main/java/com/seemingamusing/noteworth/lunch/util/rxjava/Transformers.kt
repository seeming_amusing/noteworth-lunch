package com.seemingamusing.noteworth.lunch.util.rxjava

import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface Transformers {
  fun <T> forSingle(): SingleTransformer<T, T>
}

object DefaultTransformers : Transformers {

  private val offThread = Schedulers.io()
  private val onThread = AndroidSchedulers.mainThread()

  override fun <T> forSingle() = SingleTransformer<T, T> {
    it.subscribeOn(offThread).observeOn(onThread)
  }
}

object TestTransformers : Transformers {

  private val thread = Schedulers.trampoline()

  override fun <T> forSingle() = SingleTransformer<T, T> {
    it.subscribeOn(thread).observeOn(thread)
  }
}
