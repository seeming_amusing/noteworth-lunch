package com.seemingamusing.noteworth.lunch.presentation.listing

import com.seemingamusing.noteworth.lunch.data.RestaurantDataSource
import com.seemingamusing.noteworth.lunch.data.Sorting
import com.seemingamusing.noteworth.lunch.util.rxjava.Disposables
import com.seemingamusing.noteworth.lunch.util.rxjava.Transformers
import io.reactivex.Observable
import javax.inject.Inject

class RestaurantListingPresenter
@Inject constructor(private val view: RestaurantListing.View,
                    private val restaurantDataSource: RestaurantDataSource,
                    private val disposables: Disposables,
                    private val transformers: Transformers) : RestaurantListing.Presenter {

  override fun bind() {
    disposables += combineIntents().reduceStates()
        .subscribe({ view.render(it) },
                   { view.render(unexpectedRenderState(it)) })
  }

  private fun combineIntents(): Observable<Sorting> {
    val initialLoad = view.initialScreenIntent().map { Sorting.PROMINENCE }
    val toggleSorting = view.toggleSortingIntent()
    return Observable.merge(initialLoad, toggleSorting)
  }

  // TODO: Extract this to its own class
  private fun Observable<Sorting>.reduceStates() = flatMap {
    restaurantDataSource.browse(sorting = it)
        .compose(transformers.forSingle())
        .toObservable()
        .map { RestaurantListing.State(content = it.filter { it.isOpen }) }
        .onErrorReturn { RestaurantListing.State(error = it) }
        .startWith(RestaurantListing.State(isLoading = true))
  }

  private fun unexpectedRenderState(it: Throwable?) =
      RestaurantListing.State(error = IllegalStateException("Unexpected error", it))

  override fun unbind() {
    disposables.clear()
  }
}