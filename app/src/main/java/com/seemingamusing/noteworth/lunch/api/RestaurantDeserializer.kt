package com.seemingamusing.noteworth.lunch.api

import com.google.gson.*
import com.seemingamusing.noteworth.lunch.api.model.OpeningHours
import com.seemingamusing.noteworth.lunch.api.model.PlacesPhoto
import com.seemingamusing.noteworth.lunch.model.Address
import com.seemingamusing.noteworth.lunch.model.Photo
import com.seemingamusing.noteworth.lunch.model.Restaurant
import com.seemingamusing.noteworth.lunch.model.Size
import java.lang.reflect.Type

class RestaurantDeserializer(private val gson: Gson) : JsonDeserializer<Restaurant> {

  override fun deserialize(json: JsonElement,
                           typeOfT: Type,
                           context: JsonDeserializationContext): Restaurant =
      when (json) {
        !is JsonObject -> Restaurant()
        else -> json.toPlaceDetails(context)
      }

  private fun JsonObject.toPlaceDetails(context: JsonDeserializationContext): Restaurant {
    val id = this["id"].asString
    val placeId = this["place_id"].asString
    val name = this["name"].asString
    val phoneNumber = this["formatted_phone_number"]?.asString ?: ""
    val address = this["address_components"].deserialize(context, Address::class.java)
    val vicinity = this["vicinity"].asString
    val openingHours = this["opening_hours"].deserialize(context, OpeningHours::class.java)
    val photos = this["photos"].toPhotos()
    val rating = this["rating"]?.asFloat ?: 0.0f
    val priceLevel = this["price_level"]?.asInt
    val reviewCount = this["reviews"]?.asJsonArray?.count() ?: 0

    return Restaurant(id = id, placeId = placeId, name = name, phoneNumber = phoneNumber, address = address,
                      vicinity = vicinity, isOpen = openingHours.openNow, openingHours = openingHours.hours,
                      photos = photos, rating = rating, priceLevel = priceLevel, reviewCount = reviewCount)
  }

  private fun <T> JsonElement?.deserialize(context: JsonDeserializationContext, clazz: Class<T>): T =
      context.deserialize(this, clazz) ?: clazz.newInstance()


  private fun JsonElement?.toPhotos(): List<Photo> = when (this) {
    !is JsonArray -> emptyList()
    else -> this.map { gson.fromJson(it, PlacesPhoto::class.java).asPhoto() }
  }

  private fun PlacesPhoto.asPhoto() = Photo(id = reference, size = Size(width, height))
}