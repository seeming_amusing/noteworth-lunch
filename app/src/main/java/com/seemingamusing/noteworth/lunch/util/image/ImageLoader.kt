package com.seemingamusing.noteworth.lunch.util.image

import android.content.Context
import android.support.annotation.DrawableRes
import android.widget.ImageView
import com.seemingamusing.noteworth.lunch.model.Size

interface ImageLoader {

  fun loadImage(into: ImageView,
                imageId: String,
                withContext: Context,
                maxSize: Size = Size())

  fun loadImage(into: ImageView,
                imageId: String,
                withContext: Context,
                @DrawableRes withPlaceholder: Int,
                maxSize: Size = Size())
}