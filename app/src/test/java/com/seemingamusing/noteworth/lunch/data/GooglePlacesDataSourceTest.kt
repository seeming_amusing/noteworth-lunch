@file:Suppress("IllegalIdentifier")

package com.seemingamusing.noteworth.lunch.data

import com.nhaarman.mockito_kotlin.*
import com.seemingamusing.noteworth.lunch.api.PlacesApi
import com.seemingamusing.noteworth.lunch.api.model.BrowseResponse
import com.seemingamusing.noteworth.lunch.api.model.DetailsResponse
import com.seemingamusing.noteworth.lunch.model.Restaurant
import com.seemingamusing.noteworth.lunch.util.ExceptionForTests
import com.seemingamusing.noteworth.lunch.util.rxjava.ResponseErrorException
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class GooglePlacesDataSourceTest {

  @Mock private lateinit var placesApi: PlacesApi

  private lateinit var dataSource: GooglePlacesDataSource

  @Before fun setUp() {
    MockitoAnnotations.initMocks(this)

    dataSource = GooglePlacesDataSource(placesApi)
  }

  @Test fun `should call pass in expected parameters when calling browse for prominence`() {
    val sorting = Sorting.PROMINENCE
    val keyword = "keyword"
    assumeBrowseFails()

    dataSource.browse(sorting = sorting, keyword = keyword).test()

    verify(placesApi).browse(any(), eq(sorting.value), eq(5000), eq(keyword))
  }

  @Test fun `should call pass in expected parameters when calling browse for distance`() {
    val sorting = Sorting.DISTANCE
    val keyword = "keyword"
    assumeBrowseFails()

    dataSource.browse(sorting = sorting, keyword = keyword).test()

    verify(placesApi).browse(any(), eq(sorting.value), isNull(), eq(keyword))
  }

  private fun assumeBrowseFails() {
    whenever(placesApi.browse(any(), any(), anyOrNull(), anyOrNull()))
        .thenReturn(Single.error(ExceptionForTests))
  }

  @Test fun `should fail with expected exception when API throws exception on browse`() {
    assumeBrowseFails()

    val observer = dataSource.browse().test()

    observer.assertError(ExceptionForTests)
  }

  @Test fun `should emit results from response when browse status is OK`() {
    val results = listOf(Restaurant())
    assumeBrowse(returns = BrowseResponse(status = "OK", results = results))

    val observer = dataSource.browse().test()

    observer.assertValue(results)
  }

  private fun assumeBrowse(returns: BrowseResponse) {
    whenever(placesApi.browse(any(), any(), anyOrNull(), anyOrNull())).thenReturn(Single.just(returns))
  }

  @Test fun `should emit empty results when browse status is not ZERO_RESULTS`() {
    assumeBrowse(returns = BrowseResponse(status = "ZERO_RESULTS"))

    val observer = dataSource.browse().test()

    observer.assertValue(emptyList())
  }

  @Test fun `should fail with response error exception when receiving other browse status`() {
    val message = "error message"
    assumeBrowse(returns = BrowseResponse(status = "REQUEST_DENIED", errorMessage = message))

    val observer = dataSource.browse().test()

    observer.assertError { it is ResponseErrorException && it.message!!.endsWith(message) }
  }

  @Test fun `should call pass in expected parameters when calling get`() {
    val key = "key"
    assumeGetFails()

    dataSource[key].test()

    verify(placesApi)[key]
  }

  private fun assumeGetFails() {
    whenever(placesApi[any()]).thenReturn(Single.error(ExceptionForTests))
  }

  @Test fun `should fail with expected exception when API throws exception on get`() {
    assumeGetFails()

    val observer = dataSource["key"].test()

    observer.assertError(ExceptionForTests)
  }

  @Test fun `should emit results from response when get status is OK`() {
    val result = Restaurant()
    assumeGet(returns = DetailsResponse(status = "OK", result = result))

    val observer = dataSource["key"].test()

    observer.assertValue(result)
  }

  private fun assumeGet(returns: DetailsResponse) {
    whenever(placesApi[any()]).thenReturn(Single.just(returns))
  }

  @Test fun `should fail with response error exception when get status is ZERO_RESULTS`() {
    assumeGet(returns = DetailsResponse(status = "ZERO_RESULTS"))

    val observer = dataSource["key"].test()

    observer.assertError { it is ResponseErrorException && it.message!!.endsWith("No restaurant found") }
  }

  @Test fun `should fail with response error exception when get status is NOT_FOUND`() {
    assumeGet(returns = DetailsResponse(status = "NOT_FOUND"))

    val observer = dataSource["key"].test()

    observer.assertError { it is ResponseErrorException && it.message!!.endsWith("No restaurant found") }
  }

  @Test fun `should fail with response error exception when receiving other get status`() {
    val message = "error message"
    assumeGet(returns = DetailsResponse(status = "INVALID_REQUEST", errorMessage = message))

    val observer = dataSource["key"].test()

    observer.assertError { it is ResponseErrorException && it.message!!.endsWith(message) }
  }
}