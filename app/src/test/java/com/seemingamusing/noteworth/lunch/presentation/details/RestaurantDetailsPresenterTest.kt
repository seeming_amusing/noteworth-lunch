@file:Suppress("IllegalIdentifier")

package com.seemingamusing.noteworth.lunch.presentation.details

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.seemingamusing.noteworth.lunch.data.RestaurantDataSource
import com.seemingamusing.noteworth.lunch.model.Restaurant
import com.seemingamusing.noteworth.lunch.util.ExceptionForTests
import com.seemingamusing.noteworth.lunch.util.rxjava.Disposables
import com.seemingamusing.noteworth.lunch.util.rxjava.TestTransformers
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class RestaurantDetailsPresenterTest {

  @Mock private lateinit var view: RestaurantDetails.View
  @Mock private lateinit var restaurantDataSource: RestaurantDataSource
  @Mock private lateinit var disposables: Disposables

  private lateinit var presenter: RestaurantDetailsPresenter

  @Before fun setUp() {
    MockitoAnnotations.initMocks(this)

    presenter = RestaurantDetailsPresenter(view, restaurantDataSource, disposables, TestTransformers)
  }

  @Test fun `should clear disposables when unbinding`() {
    presenter.unbind()

    verify(disposables).clear()
  }

  @Test fun `should emit loading state when binding`() {
    assumeViewHasInitialState()
    assumeDataSource(has = Restaurant())

    presenter.bind()

    verify(view).render(RestaurantDetails.State(isLoading = true))
  }

  private fun assumeViewHasInitialState() {
    whenever(view.initialScreenIntent()).thenReturn(Single.just(Restaurant()))
  }

  private fun assumeDataSource(has: Restaurant) {
    whenever(restaurantDataSource[any()]).thenReturn(Single.just(has))
  }

  @Test fun `should emit state with expected content when binding`() {
    val restaurant = Restaurant()
    assumeViewHasInitialState()
    assumeDataSource(has = restaurant)

    presenter.bind()

    verify(view).render(RestaurantDetails.State(content = restaurant))
  }

  @Test fun `should emit error state when binding`() {
    assumeViewHasInitialState()
    assumeDataSourceFails()

    presenter.bind()

    verify(view).render(RestaurantDetails.State(error = ExceptionForTests))
  }

  private fun assumeDataSourceFails() {
    whenever(restaurantDataSource[any()]).thenReturn(Single.error(ExceptionForTests))
  }
}